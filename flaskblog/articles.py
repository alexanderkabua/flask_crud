
import jwt

import datetime
from functools import wraps

from flask import jsonify,request,Blueprint

from   flaskblog import app,db

from flaskblog.models import User,Article

from flaskblog.utils import *


import uuid


articleBlueprint=Blueprint("articleBlueprint",__name__)



@articleBlueprint.route("/testRoute")
def test():
	return "loud and clear"



@articleBlueprint.route("/createArticle",methods=["POST"])
@token_required
def createArticle(user):

	data=request.get_json()

	try:

		title=data["title"]
		description=data["description"]

	except:
		return jsonify({"message":"title and description required"}),401


	article_id=str(uuid.uuid4())
	newArticle=Article(article_id=article_id,title=title,description=description,creator_id=user.id)

	try:
		

		db.session.add(newArticle)
		db.session.commit()
	except:
		return jsonify({"message":"user could not be created"}),401

	article=Article.query.filter_by(article_id=article_id).first().articleObject()



	return jsonify({"message":article}),201

@articleBlueprint.route("/getAllArticles",methods=["GET"])
def getArticles():

	articlesList=[]


	articles=Article.query.all()

	print(f'the length off articles is  {len(articles)}')

	if len(articles)<1:

		print("failed")
		return jsonify({"message":"no article exists"})
	try:
		for article in articles:
			modifiedArticle=article.articleObject()
			articlesList.append(modifiedArticle)


	# except Exception as e:
	# 	return str(e)

	except:
		return jsonify({"message":"articles  were not found"}),401


	return jsonify({"articles":articlesList})

@articleBlueprint.route("/getOneArticle/<id>",methods=["GET"])
def getArticle(id):
	try:
		article=Article.query.filter_by(article_id=id).first()

		if article is None:
			return jsonify({"message":"Article does not exists"}),401

		modifiedArticle=article.articleObject()


	except:
		return jsonify({"message":"Article could not be found"})

	return jsonify({"article":modifiedArticle})

@articleBlueprint.route("/deleteArticle/<id>",methods=["DELETE"])
@token_required
def deleteArticle(user,id):

	try:
		article=Article.query.filter_by(article_id=id).first()

		if article is None:
			return jsonify({"message":"Article does exists"})
	except:
		return jsonify({"message":"article not found"}),401

	if  (article.creator.public_id == user.public_id):
		db.session.delete(article)
		db.session.commit()

		return jsonify({"message":"article successfully deleted"}),200


	return jsonify({"message":"Action is forbidden"}),403



@articleBlueprint.route("/updateArticle/<id>",methods=["PUT"])
@token_required
def updateArticle(user,id):

	data=request.get_json()

	try:
		article=Article.query.filter_by(article_id=id).first()
		article.title=data["title"]
		article.description=data["description"]
		db.session.commit()

	except:
		return jsonify({"message":"Article does not exists"}),401

	new_Article=Article.query.filter_by(article_id=id).first()
	sendJson=new_Article.articleObject()


	return jsonify(sendJson)



	

	# return jsonify({"message":jsonify(article.articleObject())})