import jwt

import datetime
from functools import wraps

from flask import jsonify,request,Blueprint

from   flaskblog import app,db

# from flaskblog import create_app as app

from flaskblog.models import User,Article

from werkzeug.security import generate_password_hash,check_password_hash
import uuid


# from flaskblog.utils import token_required

from flaskblog.utils import *


userBlueprint=Blueprint("userBlueprint",__name__)




@userBlueprint.route("/test")
def testApi():
	return jsonify({
		"message":"loud and clear"
		})

@userBlueprint.route("/")
@token_required
def home(user):

	sendUser=user.objectData()
	return jsonify({"message":sendUser})



@userBlueprint.route("/signup",methods=["POST"])
def signup():

	data=request.get_json()





	try:

		# also try to validate the password  use decorator
		email=data["email"]
		password=data["password"]



	except:

		return jsonify({"message":"email and password required"}),401


	user=User.query.filter_by(email=email).first()

	print(user)

	if user is None:
		password=generate_password(password)


		register_user(email,password)

		uid=str(uuid.uuid4())

		new_user=User(public_id=uid,email=email,password=password)


		try:
			db.session.add(new_user)
			db.session.commit()

		except:
			return jsonify({"message":"user could not created"}),401


		return jsonify({"message":"user successfully created"}),201

	return jsonify({"message":"user already  exists"}),401

		

	






		


@userBlueprint.route("/checkArticles")
def check():
	article_list={}

	# arti=Article.query.filter_by(creator.email=="alexanderkabua@gmail.com").all()
	# usez=User.query.filter_by(articles)
	arti=User.query.filter_by(email="alexanderkabua@gmail.com").first()
	art=arti.articles

	# creator
	lt=len(art)
	return jsonify({"Articles":lt})


	
@userBlueprint.route("/signin/<name>")
def signin(name):
	return "This is the signnin page"

@userBlueprint.route("/login",methods=["POST"])
def login():

	# expect json data

	data=request.get_json()



	try:

		email=data["email"]
		password=data["password"]

	except:
		return jsonify({"message":"email and password required"}),401





	user=User.query.filter_by(email=email).first()


	if not user:
		return jsonify({"message":"user does exists"}),401
		
	validity=check_password_hash(user.password,password)

	if(validity==False):

		return jsonify({"message":"error1 in user or password"}), 401




	try:
		token=jwt.encode({"data":user.public_id,
			"exp":datetime.datetime.utcnow()+datetime.timedelta(minutes=30)
			},"secret",).decode("utf-8")
	except:
		return jsonify({"message":"error in token"}), 401


	return jsonify({
		"token":token
		})

# delete user 

@userBlueprint.route("/deleteUser/<public_id>",methods=["DELETE"])

def deleteUser(public_id):

	print(public_id)
	print("Works")


	# usertest=User.query.filter_by(public_id="bf9bf69f-1c77-4501-ad3b-609e8937c040").first()
	# print(usertest)

	try:
		userToDelete=User.query.filter_by(public_id=public_id).first()
		db.session.delete(userToDelete)
		db.session.commit()

	except:
		return jsonify({"message":"could not complete Action"})


	

	

	# try:
	# 	user=User.query.filter_by(public_id=public_id).first()
	# 	db.session.delete(user)
	# 	db.session.commit()

	# except:
	# 	return jsonify({"message":"edwfwf"}),401

	return jsonify({"message":public_id}),200




	# check if user exists
	

@userBlueprint.route("/promoteUser/<public_id>",methods=["PUT"])
def promote(public_id):
	
	user=user_exists(public_id)

	if user:
		user.role=True
		db.session.commit()

		return jsonify({"message":"successfully promoted user"}),200

	return jsonify({"message":"user not found"}),401


	

	

# @userBlueprint.route()
# get all users 
@userBlueprint.route("/getAllUser",methods=["GET"])
def getUsers():

	user_lists=[]
	users=User.query.all()
	try:
		for user in users:
			# article_list=Articles.query.filter_by()
			new_user=user.objectData()
			user_lists.append(new_user)
		
	except:
		return jsonify({"message":"an error occurred"}),401

		# print(new_user)
		

	return jsonify({"users":user_lists}),200



@app.route("/getSpecificUser/<string:id>",methods=["GET"])
def getOneUser(id):

	user=user_exists(id)

	if  user:


		user_to_return=user.objectData()

		return jsonify(user_to_return),200


	return jsonify({"message":"user does not exists"})
	# return(user_to_return)